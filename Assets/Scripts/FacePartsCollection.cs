using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="TWNS/FacePartsCollection")]
public class FacePartsCollection : ScriptableObject
{
    public List<Texture> hairParts;
    public List<Texture> glassParts;
    public List<Texture> beardParts;
}
