using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject rowPrefab;
    [SerializeField] private GameObject facePrefab;
    
    [SerializeField] private Transform rowsTransform;


    private readonly int[] _dimensions = new[] {3, 3};

    private List<Face> _faces;
    
    private void Awake()
    {
        InitFaces();
        RandomizeFaces();
    }

    private void InitFaces()
    {
        if (_faces != null) return;
        
        _faces = new List<Face>();
        
        for (var i = 0; i < _dimensions[0]; i++)
        {
            var row = Instantiate(rowPrefab, rowsTransform).transform;
            for (var j = 0; j < _dimensions[1]; j++)
            {
                _faces.Add(Instantiate(facePrefab, row).GetComponent<Face>());
            }
        }
    }

    private void RandomizeFaces()
    {
        var numberOfFaces = _faces.Count;
        var facesAsStrings = new List<string>();
        
        var twins = new List<int>();
        while (twins.Count < 2)
        {
            var rnd = Random.Range(0, numberOfFaces);
            if (twins.Contains(rnd)) continue;
            twins.Add(rnd);
        }

        var twinIds = RandomIds();
        facesAsStrings.Add(IdsToString(twinIds));
        
        for (var i = 0; i < numberOfFaces; i++)
        {
            if (twins.Contains(i))
            {
                _faces[i].ChangeFace(twinIds);
                continue;
            }

            do
            {
                var ids = RandomIds();
                if (facesAsStrings.Contains(IdsToString(ids))) continue;
                facesAsStrings.Add(IdsToString(ids));
                _faces[i].ChangeFace(ids);
                break;
            } while (true);
        }
    }
    private static int[] RandomIds()
    {
        var ret = new int[3];
        for (var i = 0; i < 3; i++)
        {
            var rnd = Random.Range(0, 4);
            ret[i] = rnd;
        }

        return ret;
    }

    private static string IdsToString(IReadOnlyList<int> ids) => $"{ids[0]}{ids[1]}{ids[2]}";


}
