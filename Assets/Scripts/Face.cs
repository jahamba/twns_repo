using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Face : MonoBehaviour
{
    [SerializeField] private FacePartsCollection facePartsCollection;

    [SerializeField] private RawImage hairRawImage;
    [SerializeField] private RawImage glassRawImage;
    [SerializeField] private RawImage beardRawImage;
    
    public void ChangeFace(int[] ids)
    {
        hairRawImage.texture = facePartsCollection.hairParts[ids[0]];
        glassRawImage.texture = facePartsCollection.glassParts[ids[1]];
        beardRawImage.texture = facePartsCollection.beardParts[ids[2]];
    }
}
